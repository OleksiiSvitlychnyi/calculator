
const calculator = {
    operand1: "",
    sign: "",
    operand2: "",
    rez: "",
    mem: "",
};

const rezultButton = document.getElementById("rez");
const valueDisplay = document.querySelector(".display input");
const buttons = document.querySelector(".keys");
const memory = document.getElementById("mem");
const memoryPlus = document.getElementById("mem-plus");
const memoryMinus = document.getElementById("mem-minus");
const mrcButton = document.getElementById("mrc");


// Показываю М+ и М- и записываю число в mem:


const showMemoryPlus = function() {
    if(memory.style.display === "block") {
         memory.style.display = "none"
    } else {
     memory.style.display = "block";
    }

    calculator.mem = valueDisplay.value;
    console.log(calculator.mem)
 }

 const showMemoryMinus = function() {
     if(memory.style.display === "block") {
          memory.style.display = "none"
     } else {
      memory.style.display = "block";
     }

     calculator.mem = "";
     console.log(calculator.mem)
  }

  memoryPlus.addEventListener("click", showMemoryPlus);
  memoryMinus.addEventListener("click", showMemoryMinus);



// Калькулятор (Условия):-------------------------------------------------------------------------


// Вывод элемента на экран калькулятора:


function showButtonsValue(element) {
    valueDisplay.value = element;
};

// Проверка ввода: 


const validate = function(pattern, text) {
    return pattern.test(text);
}


// События мыши:---------------------------------------------------------------------------------


buttons.addEventListener("click", (e) => {


    // Операнд 1:

    
    if(validate(/[0-9.]/, e.target.value) && calculator.sign === "") {

        calculator.operand1 += e.target.value;
        showButtonsValue(calculator.operand1);

        console.log("operator_1 = " + calculator.operand1);
    } 


    // Знак:


    else if(validate(/[+-/*]/, e.target.value) && calculator.operand1 !== "" && calculator.sign === "" && calculator.operand2 === "") {

        if(calculator.sign === "") {
            calculator.sign += e.target.value; 
            console.log("sign = " + calculator.sign); 
        };
    } 


    // Знак. Условие 2: 


    else if(validate(/[+-/*]/, e.target.value) && calculator.operand2 !== "" && calculator.sign !== "" && calculator.operand1 !== "" && e.target.value !== ".") {
        
            calculator.rez = calculate(calculator.operand1, calculator.operand2, calculator.sign);
            showButtonsValue(calculator.rez);
            calculator.operand1 = calculator.rez;
            calculator.operand2 = "";
            calculator.sign = e.target.value; 
            rezultButton.setAttribute("disabled", "disabled"); 
    }


    // Операнд 2:


    else if(validate(/[0-9.]/, e.target.value) && calculator.operand1 !== "" && calculator.sign !== "") {

        rezultButton.removeAttribute("disabled");
        calculator.operand2 += e.target.value;
        showButtonsValue(calculator.operand2);

        console.log("operator_2 = " + calculator.operand2);
    } 


    // Равно:


    else if(validate(/^=$/, e.target.value)) {

        if(calculator.operand2 === "0" && calculator.sign === "/") {
            calculator.rez = calculate(calculator.operand1, calculator.operand2, calculator.sign);
            calculator.operand1 = "";
            calculator.operand2 = "";
            calculator.sign = "";
            calculator.rez = "";
        } else {
            calculator.rez = calculate(calculator.operand1, calculator.operand2, calculator.sign);
            showButtonsValue(calculator.rez);
            calculator.operand1 = calculator.rez;
            calculator.operand2 = "";
            calculator.sign = "";
            rezultButton.setAttribute("disabled", "disabled"); 
        }

        console.log(calculator.rez);
    }

});


 //События клавиатуры: --------------------------------------------------------

 

window.addEventListener("keypress", (e) => {


    // Операнд 1:


    if(validate(/[0-9]/, e.key) && calculator.sign === "") {

        calculator.operand1 += e.key;
        showButtonsValue(calculator.operand1);

        console.log("operator_1 = " + calculator.operand1);
    }


    //Знак :


    else if(validate(/[+-/*]/, e.key) && calculator.operand1 !== "" && calculator.sign === "" && calculator.operand2 === "") {

        if(calculator.sign === "") {
            calculator.sign += e.key;  

            console.log("sign = " + calculator.sign);
        };
        
    }


    // Знак. Условие 2: 


    else if(validate(/[+-/*]/, e.key) && calculator.operand2 !== "" && calculator.sign !== "" && calculator.operand1 !== "" && e.key !== ".") {
        
        calculator.rez = calculate(calculator.operand1, calculator.operand2, calculator.sign);
        showButtonsValue(calculator.rez);
        calculator.operand1 = calculator.rez;
        calculator.operand2 = "";
        calculator.sign = e.key; 
        rezultButton.setAttribute("disabled", "disabled"); 
        
}


// Операнд 2: 


    else if(validate(/[0-9.]/, e.key) && calculator.operand1 !== "" && calculator.sign !== "") {

        rezultButton.removeAttribute("disabled");
        calculator.operand2 += e.key;
        showButtonsValue(calculator.operand2);

        console.log("operator_2 = " + calculator.operand2);
} 


//Равно:


else if(validate(/^Enter$/, e.code) || validate(/^NumpadEnter$/, e.code)) {

    if(calculator.operand2 === "0" && calculator.sign === "/") {
        calculator.rez = calculate(calculator.operand1, calculator.operand2, calculator.sign);
        calculator.operand1 = "";
        calculator.operand2 = "";
        calculator.sign = "";
        calculator.rez = "";
    } else {
        calculator.rez = calculate(calculator.operand1, calculator.operand2, calculator.sign);
        showButtonsValue(calculator.rez);
        calculator.operand1 = calculator.rez;
        calculator.operand2 = "";
        calculator.sign = "";
        rezultButton.setAttribute("disabled", "disabled"); 
    }
    
    console.log(calculator.rez);
    }

})


// Функция расчета:


    const calculate = function(operand1, operand2, sign) {

        switch(sign) {

            case "+" : return Number((Number(operand1) + Number(operand2)).toFixed(14));

            case "-" : return Number((operand1 - operand2).toFixed(14));
            
            case "*" : return Number((operand1 * operand2).toFixed(14));
            
            case "/" : 
                        if(operand2 === "0") {
                           operand1 = "";
                           operand2 = "";
                           console.log("error");
                           showButtonsValue("error");
                           break;
                        }else {
                            return Number((operand1 / operand2).toFixed(14));
                        }
                       
        }
    };

    
    // Очистка экрана С:


    const с = document.getElementById("c");
    
    с.addEventListener("click", () => {
        valueDisplay.value = "";
        calculator.operand1 = "";
        calculator.operand2 = "";
        calculator.sign = "";
    });

    // Кнопка MRC:

    mrcButton.addEventListener("click", () => {
        valueDisplay.value = calculator.mem;
        calculator.operand1 = valueDisplay.value;
    });


   


    


   

    




    




